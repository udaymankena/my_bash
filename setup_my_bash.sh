#!/bin/bash

home_dir=~

# clone my_bash repo to my_bash_dir
my_bash_dir="$home_dir/my_bash"
rm -rf $my_bash_dir
mkdir $my_bash_dir

echo "cloning my_bash repo....."
git clone git@gitlab.com:udaymankena/my_bash.git $my_bash_dir


# move my_bash/.zshrc to the user home directory
if [[ -f "~/.zshrc" ]]; then
  rm ~/.zshrc
fi

cp $my_bash_dir/.zshrc $home_dir/
