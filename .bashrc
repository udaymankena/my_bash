#!/bin/bash
my_bash_dir="$HOME/Code/personal/Gitlab/my_bash"
my_utils_dir="$HOME/Code/personal/Gitlab/my_utils"

# sourcing aliases
source $my_bash_dir/.aliases

#sourcing utils
source $my_utils_dir/Shell/shell_utils.sh

#welcome message
source $my_bash_dir/welcome_message.sh

# PATH
export PATH=$PATH:$HOME/Software_libraries/sbt/bin
# setting 1.8 as default java version
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
