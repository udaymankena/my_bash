#!/usr/bin/env bash

############ sync remote my_bash repo with the local ###################

local_my_bash_dir="$HOME/my_bash"
temp_dir="$HOME/my_bash_temp"
mkdir "$temp_dir"

# pull the remote version to a temp dir
git clone git@gitlab.com:udaymankena/my_bash.git $temp_dir

# syncing the temp dir with my_bash
rsync -av --delete --dry-run $temp_dir/ $local_my_bash_dir # printing sync result
rsync -av --delete $temp_dir/ $local_my_bash_dir