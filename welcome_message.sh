#!/bin/bash

# Welcome message to display on a new terminal window
# font color : green
color='\e[0;32m'

# font color : white
NC='\e[0m'

# Array containing quotes

quotes=(" Ever tried. Ever failed. No matter. Try Again. Fail again. Fail better.
\n \t\t\t\t\t\t\t-Samuel Beckett " "Never give up, for that is just the place and time that the tide will turn.
\n \t\t\t\t\t\t\t-Harriet Beecher Stowe " "Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.
\n \t\t\t\t\t\t\t-Thomas A. Edison" "Life isn't about getting and having, it's about giving and being.
\n \t\t\t\t\t\t\t-Kevin Kruse" "Strive not to be a success, but rather to be of value.
\n \t\t\t\t\t\t\t-Albert Einstein" "You miss 100% of the shots you don't take.
\n \t\t\t\t\t\t\t-Wayne Gretzky" "People who are unable to motivate themselves must be content with mediocrity, no matter how impressive their other talents.
\n \t\t\t\t\t\t\t-Andrew Carnegie" "Design is not just what it looks like and feels like. Design is how it works.
\n \t\t\t\t\t\t\t-Steve Jobs" "Only those who dare to fail greatly can ever achieve greatly.
\n \t\t\t\t\t\t\t-Robert F. Kennedy" "All our dreams can come true, if we have the courage to pursue them.
\n \t\t\t\t\t\t\t-Walt Disney " "Success consists of going from failure to failure without loss of enthusiasm.
\n \t\t\t\t\t\t\t-Winston Churchill" "Discipline is doing what needs to be done even if you don't want to.
\n \t\t\t\t\t\t\t--anonymous" "The mark of a good programmer is not the absence of bugs, but the presence of clear intentions"
 "At the end of the day, we know how much money we have in the bank, but have no idea how much time we are left with…")

# To extract ones digit of second from system clock
random_num=`date|cut -d " " -f 4|cut -d ":" -f 3`
quotes_len="${#quotes[@]}"
quote_index=`echo "$random_num % $quotes_len" | bc`

echo -e "--------------------------------------------------------------------------------"
echo -e "****************             Welcome back `whoami`!             *****************"
echo  -e "\n${quotes[$quote_index]}"
echo -e "--------------------------------------------------------------------------------"

#end of code



##########################################################
